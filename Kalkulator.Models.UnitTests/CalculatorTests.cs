﻿using NUnit.Framework;
using System;

namespace Kalkulator.Models.UnitTests
{
    [TestFixture]
    public class CalculatorTests
    {
        [TestCase(2, 2, 4)]
        [TestCase(2, -3, -1)]
        [TestCase(-2, 3, 1)]
        [TestCase(-2, -2, -4)]
        public void Add_AddingTwoValues_AddResult(double firstNumber, double secondNumber, 
            double additionResult)
        {
            // Arrange
            Calculator calculator = new Calculator();

            // Act
            double result = calculator.Add(firstNumber, secondNumber);

            // Assert
            Assert.AreEqual(additionResult, result);
        }

        [TestCase(3, 2, 1)]
        [TestCase(4, -2, 6)]
        [TestCase(-2, 2, -4)]
        [TestCase(-3, -2, -1)]
        public void Sub_subtractionTwoValues_SubtractionResult
            (double firstNumber, double secondNumber, double subtractionResult)
        {
            // Arrange
            Calculator calculator = new Calculator();

            // Act
            double result = calculator.Sub(firstNumber, secondNumber);

            // Assert
            Assert.AreEqual(subtractionResult, result);
        }

        [TestCase(2, 2, 4)]
        [TestCase(2, -2, -4)]
        [TestCase(-2, 2, -4)]
        [TestCase(-2, -2, 4)]
        public void Mul_MultiplicationTwoValues_MultiplicationResult
            (double firstNumber, double secondNumber, double multiplicationResult)
        {
            // Arrange
            Calculator calculator = new Calculator();

            // Act
            double result = calculator.Mul(firstNumber, secondNumber);

            // Assert
            Assert.AreEqual(multiplicationResult, result);
        }

        [TestCase(10, 2, 5)]
        [TestCase(-4, 2, -2)]
        [TestCase(4, -2, -2)]
        [TestCase(5, 2, 2.5)]
        [TestCase(1, 3, 0.3333333333333333)]
        public void Div_DivisionTwoValues_DivisionResult
            (double firstNumber, double secondNumber, double divisionResult)
        {
            // Arrange
            Calculator calculator = new Calculator();
            
            // Act
            double result = calculator.Div(firstNumber, secondNumber);

            // Assert
            Assert.AreEqual(divisionResult, result);
        }

        [Test]
        public void Div_valueDividedByZero_TrowhAnException()
        {
            // Arrange
            Calculator calculator = new Calculator();

            // Act

            // Assert
            Assert.Throws<DivideByZeroException>(() => calculator.Div(2, 0));
        }
    }
}