﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalkulator.Models
{
    public class Calculator
    {
        public double NumberOne { get; set; }
        public double NumberTwo { get; set; }
        public OperandType? OperandType { get; set; }

        public double Add(double numberOne, double numberTwo)
        {
            return numberOne + numberTwo;
        }

        public double Sub(double numberOne, double numberTwo)
        {
            return numberOne - numberTwo;
        }

        public double Mul(double numberOne, double numberTwo)
        {
            return numberOne * numberTwo;
        }

        public double Div(double numberOne, double numberTwo)
        {
            if (numberTwo == 0) throw new DivideByZeroException();
            return numberOne / numberTwo;
        }
    }
}
