﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Kalkulator.ViewModels.Commands
{
    public class ClearCommand
        : ICommand
    {
        private readonly KalkulatorViewModel vm;

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            vm.ScreenContent = string.Empty;
        }

        public ClearCommand(KalkulatorViewModel vm)
        {
            this.vm = vm;
        }
    }
}
