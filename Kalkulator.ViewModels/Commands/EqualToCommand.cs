﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Kalkulator.ViewModels.Commands
{
    public class EqualToCommand
        : ICommand
    {
        private KalkulatorViewModel vm;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            double result;

            if (!string.IsNullOrEmpty(vm.ScreenContent) &&
               double.TryParse(vm.ScreenContent, NumberStyles.Number, CultureInfo.InvariantCulture, out result))
            {
                if (vm.Kalkulator.OperandType == Models.OperandType.Div && double.Parse(vm.ScreenContent) == 0)
                    return false;

                return true;
            }
            return false;
        }

        public void Execute(object parameter)
        {
            vm.Kalkulator.NumberTwo = double.Parse(vm.ScreenContent, CultureInfo.InvariantCulture);
            
            switch (vm.Kalkulator.OperandType)
            {
                case Models.OperandType.Add:
                    {
                        double result = vm.Kalkulator.NumberOne + vm.Kalkulator.NumberTwo;
                        vm.ScreenContent = result.ToString();
                        break;
                    }

                case Models.OperandType.Sub:
                    {
                        double result = vm.Kalkulator.NumberOne - vm.Kalkulator.NumberTwo;
                        vm.ScreenContent = result.ToString();
                        break;
                    }

                case Models.OperandType.Mul:
                    {
                        double result = vm.Kalkulator.NumberOne * vm.Kalkulator.NumberTwo;
                        vm.ScreenContent = result.ToString();
                        break;
                    }

                case Models.OperandType.Div:
                    {
                        double result = vm.Kalkulator.NumberOne / vm.Kalkulator.NumberTwo;
                        vm.ScreenContent = result.ToString();
                        break;
                    }
            }
        }

        public EqualToCommand(KalkulatorViewModel model)
        {
            this.vm = model;
        }
    }
}
