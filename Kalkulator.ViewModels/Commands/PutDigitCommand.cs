﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Kalkulator.ViewModels.Commands
{
    public class PutDigitCommand
        : ICommand
    {
        private readonly KalkulatorViewModel vm;
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            vm.ScreenContent += parameter.ToString();
        }

        public PutDigitCommand(KalkulatorViewModel vm)
        {
            this.vm = vm;
        }
    }
}
