﻿using Kalkulator.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Kalkulator.ViewModels.Commands
{
    public class PutOperandCommand
        : ICommand
    {
        private KalkulatorViewModel vm;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            double result;
            return !string.IsNullOrEmpty(vm.ScreenContent) && 
                double.TryParse(vm.ScreenContent, NumberStyles.Number, CultureInfo.InvariantCulture, out result);
        }

        public void Execute(object parameter)
        {
            vm.Kalkulator.OperandType = (OperandType) Enum.Parse(typeof(OperandType), parameter.ToString());
            vm.Kalkulator.NumberOne = double.Parse(vm.ScreenContent, CultureInfo.InvariantCulture);
            vm.ScreenContent = string.Empty;
        }

        public PutOperandCommand(KalkulatorViewModel model)
        {
            this.vm = model;
        }
    }
}
