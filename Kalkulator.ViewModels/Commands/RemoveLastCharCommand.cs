﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Kalkulator.ViewModels.Commands
{
    public class RemoveLastCharCommand
        : ICommand
    {
        private readonly KalkulatorViewModel vm;
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            return !string.IsNullOrEmpty(vm.ScreenContent);
        }

        public void Execute(object parameter)
        {
            vm.ScreenContent = vm.ScreenContent.Remove(vm.ScreenContent.Length - 1);
        }

        public RemoveLastCharCommand(KalkulatorViewModel vm)
        {
            this.vm = vm;
        }
    }
}
