﻿using Kalkulator.Models;
using Kalkulator.ViewModels.Commands;
using Kalkulator.ViewModels.Helpers;

namespace Kalkulator.ViewModels
{
    public class KalkulatorViewModel
        : ObservedObject
    {
        private Calculator kalkulator = new Calculator();

        public Calculator Kalkulator => kalkulator;

        private string screenContent;

        public string ScreenContent
        {
            get => screenContent;
            set
            {
                if (screenContent != value)
                {
                    screenContent = value;
                    OnPropertyChanged(nameof(ScreenContent));
                }
            }
        }

        public PutDigitCommand PutDigitCommand { get; set; }

        public EqualToCommand EqualToCommand { get; set; }

        public ClearCommand ClearCommand { get; set; }

        public PutOperandCommand PutOperandCommand { get; set; }

        private RemoveLastCharCommand removeLastCharCommand;
        public RemoveLastCharCommand RemoveLastCharCommand
        {
            get => removeLastCharCommand;
            set
            {
                if (removeLastCharCommand != value)
                {
                    removeLastCharCommand = value;
                    OnPropertyChanged(nameof(RemoveLastCharCommand));
                }
            }
        }

        public KalkulatorViewModel()
        {
            PutDigitCommand = new PutDigitCommand(this);
            ClearCommand = new ClearCommand(this);
            RemoveLastCharCommand = new RemoveLastCharCommand(this);
            PutOperandCommand = new PutOperandCommand(this);
            EqualToCommand = new EqualToCommand(this);
        }



    }
}
